module gitlab.com/felipemocruha/dynconf

go 1.15

require (
	github.com/dgraph-io/ristretto v0.0.3
	github.com/gomodule/redigo v1.8.3
	github.com/json-iterator/go v1.1.10
	gitlab.com/felipemocruha/anomalies v0.1.2
)
