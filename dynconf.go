package dynconf

import (
	"bytes"
	json "github.com/json-iterator/go"
	"time"

	"github.com/dgraph-io/ristretto"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/felipemocruha/anomalies"
)

type Store interface {
	Get(key string) (interface{}, anomalies.Anomaly)
	Set(key string, value interface{}) anomalies.Anomaly
	SetWithTTL(key string, value interface{}, ttl time.Duration) anomalies.Anomaly
	Del(key string) anomalies.Anomaly
	Clean() anomalies.Anomaly
	Close() anomalies.Anomaly
}

type Config struct {
	RedisHost     string
	RedisPassword string
}

func newLocal() (*ristretto.Cache, error) {
	return ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e7,
		MaxCost:     1 << 30,
		BufferItems: 64,
	})
}

func NewStore(config *Config) (Store, anomalies.Anomaly) {
	local, err := newLocal()
	if err != nil {
		return nil, anomalies.Internal(err)
	}

	return &Redis{
		&redis.Pool{
			Dial: func() (redis.Conn, error) {
				return redis.Dial(
					"tcp",
					config.RedisHost,
					redis.DialPassword(config.RedisPassword),
				)
			},
		},
		local,
	}, nil
}

type Redis struct {
	pool  *redis.Pool
	local *ristretto.Cache
}

func (c *Redis) Get(key string) (interface{}, anomalies.Anomaly) {
	if val, found := c.local.Get(key); found {
		return val, nil
	}

	conn := c.pool.Get()
	defer conn.Close()

	buf, err := redis.Bytes(conn.Do("GET", key))
	if err == redis.ErrNil {
		return nil, anomalies.NotFound(err)
	}
	if err != nil {
		return nil, anomalies.Internal(err)
	}

	decoder := json.NewDecoder(bytes.NewReader(buf))
	var res interface{}

	if err := decoder.Decode(res); err != nil {
		return nil, anomalies.Internal(err)
	}

	c.local.Set(key, res, 1)
	return res, nil
}

func (c *Redis) Set(key string, value interface{}) anomalies.Anomaly {
	buf := &bytes.Buffer{}
	if err := json.NewEncoder(buf).Encode(value); err != nil {
		return anomalies.Internal(err)
	}

	conn := c.pool.Get()
	defer conn.Close()

	_, err := conn.Do("SET", key, buf)
	if err != nil {
		return anomalies.Internal(err)
	}
	c.local.Set(key, buf, 1)

	return nil
}

func (c *Redis) SetWithTTL(key string, value interface{}, ttl time.Duration) anomalies.Anomaly {
	buf := &bytes.Buffer{}
	if err := json.NewEncoder(buf).Encode(value); err != nil {
		return anomalies.Internal(err)
	}

	conn := c.pool.Get()
	defer conn.Close()

	_, err := conn.Do("SETEX", key, ttl.Seconds(), buf)
	if err != nil {
		return anomalies.Internal(err)
	}
	c.local.SetWithTTL(key, value, 1, ttl)

	return nil
}

func (c *Redis) Del(key string) anomalies.Anomaly {
	conn := c.pool.Get()
	defer conn.Close()

	_, err := conn.Do("DEL", key)
	if err != nil {
		return anomalies.Internal(err)
	}
	c.local.Del(key)

	return nil
}

func (c *Redis) Clean() anomalies.Anomaly {
	conn := c.pool.Get()
	defer conn.Close()

	_, err := conn.Do("FLUSHALL")
	if err != nil {
		return anomalies.Internal(err)
	}
	c.local.Clear()

	return nil
}

func (c *Redis) Close() anomalies.Anomaly {
	if err := c.pool.Close(); err != nil {
		return anomalies.Internal(err)
	}
	c.local.Close()

	return nil
}
